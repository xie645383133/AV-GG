
local anim8 = require '3rd/anim8'
local image, animation, geh_animation, geh_animation2
local geh = {}
local anim1_pos = {x=20, y=90, c=0}

local flux = require '3rd/flux'

local function update(dt)
    animation:update(dt)
 	flux.update(dt)
    geh_animation:update(dt)
    geh_animation2:update(dt)
end

local function init()
    image = love.graphics.newImage('data/img/anim1.png')
end

local function draw()
    animation:draw(image, anim1_pos.x, anim1_pos.y)
    for i = 1, 21 do
        geh_animation:draw(geh[i], (i-1)*32, 0)
        geh_animation2:draw(geh[i], 800 - i*32, 48)
    end
    love.graphics.setColor(255, 255, 0, 255)
    love.graphics.print("Welcome!", 200, 250)
    love.graphics.reset()
    love.graphics.print("Press 'Enter' to continue.", 200, 300)

    if anim1_pos.c==1 then
        anim1_pos.c=0
        flux.to(anim1_pos, 2, { x = 500 }):ease("sinein")
        flux.to(anim1_pos, 2, { y = 300 }):ease("elasticout"):delay(2)
        flux.to(anim1_pos, 2, { x = 20 }):ease("backout"):delay(4)
        flux.to(anim1_pos, 2, { y = 90, c = 1 }):ease("circin"):delay(6)
    end
end

local function keypressed(key)
    if key == 'return' then
        local s = require('engine/scene')
        s.switch('main_menu')
    end
end

local function switch_enter()
    local g = anim8.newGrid(192, 192, image:getWidth(), image:getHeight())
    animation = anim8.newAnimation(g('1-5','1-2'), 0.1)
    flux.to(anim1_pos, 2, { x = 500 }):ease("sinein"):delay(1)
    flux.to(anim1_pos, 2, { y = 300 }):ease("elasticout"):delay(3)
    flux.to(anim1_pos, 2, { x = 20 }):ease("backout"):delay(5)
    flux.to(anim1_pos, 2, { y = 90, c = 1 }):ease("circin"):delay(7)
    for i = 1,21 do
        geh[i] = love.graphics.newImage('data/img/charactor_vx/geh_'..i..'.png')
    end
    local m = anim8.newGrid(32,48, 96, 192)
    geh_animation = anim8.newAnimation(m('1-3', '3-3'), 0.1)
    geh_animation2 = anim8.newAnimation(m('1-3', '2-2'), 0.1)
end

local x = {}
x.draw = draw
x.keypressed = keypressed
x.init = init
x.update = update
x.switch_enter = switch_enter
return x
