
local scene_list = {}

scene_list['welcome']           = require 'scene/scene_welcome'
scene_list['welcome_cool']      = require 'scene/scene_welcome_cool'
scene_list['main_menu']         = require 'scene/scene_main_menu'
scene_list['about']             = require 'scene/scene_about'
scene_list['music_player']      = require 'scene/scene_music_player'
scene_list['game2048']          = require 'scene/scene_game_2048'
scene_list['game_biglittle']    = require 'scene/scene_game_biglittle'
scene_list['chapter1']          = require 'scene/scene_chapter1'
scene_list['gui_test']          = require 'scene/scene_gui_test'
scene_list['map_test']          = require 'scene/scene_map_test'
scene_list['charactor_test']    = require 'scene/scene_charactor_test'

return scene_list
