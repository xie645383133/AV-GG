
local x = {}

x.draw = function()
    loveframes.draw()
end
x.init = function()
    require("3rd.loveframes")
end

local function enter()
    loveframes.SetState("gui_test")
    local frame = loveframes.Create("frame")
    frame:SetName("GUI test")
    frame:SetState("gui_test")
    frame:CenterWithinArea(unpack({5, 40, 540, 555}))
    frame.OnClose = function(object)
        require('engine/scene').retreat()
    end
    local button = loveframes.Create("button", frame)
    button:SetWidth(200)
    button:SetText("Button")
    button:Center()
    button.OnClick = function(object, x, y)
        require('engine/scene').retreat()
    end
    button.OnMouseEnter = function(object)
        object:SetText("The mouse entered the button.")
    end
    button.OnMouseExit = function(object)
        object:SetText("The mouse exited the button.")
    end
end

local function leave()
    loveframes.SetState("none")
end

x.switch_enter = enter
x.switch_leave = leave
x.retreat_enter = enter
x.retreat_leave = leave

x.update = function(dt) loveframes.update(dt) end
x.mousepressed = function(x, y, button) loveframes.mousepressed(x, y, button) end
x.mousereleased = function(x, y, button) loveframes.mousereleased(x, y, button) end
x.keypressed = function(key, isrepeat) loveframes.keypressed(key, isrepeat) end
x.keyreleased = function(key) loveframes.keyreleased(key) end

return x
