
local x = {}

--local tx, ty = 3000, 0
local loader = require("3rd/Advanced-Tiled-Loader/loader")
loader.path = "data/map/"
local map = loader.load("map1.tmx")

x.draw = function()
    --map:autoDrawRange( math.floor(tx), math.floor(ty), 1, -100)
    map:draw()
end
x.init = nil
x.switch_enter = nil
x.switch_leave = nil
x.retreat_enter = nil
x.retreat_leave = nil
x.update = nil
x.mousepressed = nil
x.mousereleased = nil
x.keypressed = nil
x.keyreleased = nil
return x
