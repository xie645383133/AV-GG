anim8.lua
https://github.com/kikito/anim8

flux.lua
https://github.com/rxi/flux/

tween.lua
https://github.com/kikito/tween.lua

cron.lua
https://github.com/kikito/cron.lua

hump
https://github.com/vrld/hump

middleclass.lua
https://github.com/kikito/middleclass

LoveFrames
https://github.com/NikolaiResokav/LoveFrames

Advanced-Tiled-Loader
https://github.com/Kadoba/Advanced-Tiled-Loader
